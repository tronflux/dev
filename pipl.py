from piplapis.search import SearchAPIRequest
from piplapis.data import Person, Name, Address


API_KEY = 'CONTACT-PREMIUM-DEMO-k04bte6gc9ch5sgm2bzyqodk'

fields = [Name(first=u'David', last=u'Turner'),
          Address(country=u'US', state=u'NY', city=u'New York')]

request = SearchAPIRequest(person=Person(fields=fields), api_key='API_KEY')
