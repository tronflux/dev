# README #

### What is this repository for? ###

* Vagrant and Ansible files for setting up a reusable Python dev environment
* 0.0.0 beta

### How do I get set up? ###

`vagrant up`
`vagrant provision`
`vagrant ssh`
`cd ~/`
`ansible-playbook core.yml`