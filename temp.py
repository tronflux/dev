import matplotlib
matplotlib.use('Agg')
import numpy as np
import pandas as pd
# export DISPLAY=:0.0

from scipy.interpolate import CubicSpline
from matplotlib import pyplot as plt


t = np.linspace(0, 2 * np.pi, 101)
x = t / np.pi  # for x axis of plots
a = np.sin(t)  # 1 full sine wave


# def calc_integral(signal, y0, dx):
#     y = y0 + np.cumsum(signal * dx)
#     return y

cs = CubicSpline(t, x)



def calc_indef_integral(signal):
    results = []
    for measurement in signal:
        results.append(cs.antiderivative(nu=1))
    return results


dt = t[1] - t[0]
# integral(sin(t)) = -cos(t) + C
indef_integral = calc_indef_integral(a)  # integrate entire signal at once

m = 10
n = len(indef_integral) // m + 1
prev_dat = []
v0 = -1.
plt.figure()

for i in range(1, n):
    t0 = (i - 1) * m
    t1 = i * m  # right-exclude
    dx = 2 * np.pi / 100  # known constant; i.e. 1 / sampling_rate
    # convert to DataFrame to keep consistent with rep_learn2

    a_ = pd.DataFrame(a[t0:t1], index=range(t0, t1))
    if len(prev_dat) == 0:
        data = a_
        v = calc_indef_integral(data)
        # v = (10,)
        # v_ = v
        # print(dir(v_))
        x_ = x[t0:t1]
        v0 = v[-1]
        prev_dat.append(a_)  # store previous data packet
    else:
        data = pd.concat(prev_dat + [a_], axis=0)
        v_ = calc_indef_integral(data)
        x_ = x[t0 - m:t1]
        v = v_[m:]  # pull out _new_ velocity values - indices 0-9 have already been calculated
        # v = (10,)
        # v0 = v_[m][0]  # pull out velocity value at v_[10]
        prev_dat.pop(0)
        prev_dat.append(a_)  # store previous data packet
    print (v.index)
    lab = None

    if i == n - 1:
        lab = 'vel_calc'
    plt.plot(x[t0:t1], v, color='red', alpha=1, label=lab)
    plt.plot(x_, v_, alpha=0.25)
plt.plot(x, a, color='blue', linestyle='dashed', alpha=0.5, label='accel')
plt.plot(x, integral, 'black', linestyle='dashed', alpha=0.5, label='vel')
plt.legend()
plt.savefig("out1.png")
